ENVFLAG := -DDEV #-DPRODUCTION
CPPFLAGS := -std=c++14 -g -Werror -Wall $(ENVFLAG)
CXX := g++
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Linux)
	LDFLAGS := -L/usr/lib -L/usr/local/lib
	CXXFLAGS := -I/usr/include/mysql++ -I/usr/local/lib
endif
ifeq ($(UNAME_S), Darwin)
	LDFLAGS := -L/usr/local/lib -L/usr/local/mysql-connector-c-6.1.10-macos10.12-x86_64/lib
	CXXFLAGS := -I/usr/local/include/mysql++ -I/usr/local/mysql-connector-c-6.1.10-macos10.12-x86_64/include
endif
LDLIBS := -lmysqlpp -lmysqlclient
EXECUTABLE := aggregate

all: $(EXECUTABLE)

clean: 
	rm -f $(EXECUTABLE) *.o
