#include <mysql++.h>
#include <iostream>
#include <iomanip>
#include <stdint.h>
#include <vector>
#include <sstream>
#include <ctime>

using namespace std;
using namespace mysqlpp;

#ifndef PRODUCTION
const string DATABASE = "wayfinding_DEV";
#else
const string DATABASE = "wayfinding_PROD";
#endif

struct daily_shelf_summary {
    string date;
    uint32_t store_number;
    uint32_t products_added;
    uint32_t products_removed;
    uint32_t products_rearranged;
    uint32_t total_shelves;
    uint32_t total_products;
    float products_per_shelf;
    string shelf_type;
};

struct daily_adoption_summary {
    string date;
    uint32_t store_number;
    uint32_t product_lookups;
    uint32_t shelves_modified;
    uint32_t store_lookups;
};

uint32_t query_value(Connection& conn, string query_string) {
    auto query = conn.query(query_string);
    uint32_t val = 0;
    for (auto row : query.store()) {
        val = row[0];
    }
    //cerr << val << endl;
    return val;
}

uint32_t products_added(Connection& conn, uint16_t store_number, string& date, string& shelf_type) {
    ostringstream oss;
    oss << "select sum(case when ((facing_old<0) and (facing_new>0)) then 1 else 0 end) from "
        << DATABASE << ".audit"
        << " where (updatedAt >= DATE(DATE_ADD(\'" << date << "\', interval 1 day))) and "
        << "(updatedAt < DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) and (storeNumber = \'"
        << store_number << "\') and (shelf_id like \'%" << shelf_type << "%\')  group by shelf_id order by shelf_id asc";
    return query_value(conn, oss.str());
} 

uint32_t products_removed(Connection& conn, uint16_t store_number, string& date, string& shelf_type) {
    ostringstream oss;
    oss << "select sum(case when ((facing_old>0) and (facing_new<0)) then 1 else 0 end) from "
        << DATABASE << ".audit"
        << " where (updatedAt >= DATE(DATE_ADD(\'" << date << "\', interval 1 day))) and "
        << "(updatedAt < DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) and (storeNumber = \'"
        << store_number << "\') and (shelf_id like \'%" << shelf_type << "%\')  group by shelf_id order by shelf_id asc";
    return query_value(conn, oss.str());
}

uint32_t products_rearranged(Connection& conn, uint16_t store_number, string& date, string& shelf_type) {
    ostringstream oss;
    oss << "select sum(case when ((facing_old)=(facing_new) and (column_old<>column_new)) then 1 else 0 end) from "
        << DATABASE << ".audit"
        << " where (updatedAt >= DATE(DATE_ADD(\'" << date << "\', interval 1 day))) and "
        << "(updatedAt < DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) and (storeNumber = \'"
        << store_number << "\') and (shelf_id like \'%" << shelf_type << "%\')  group by shelf_id order by shelf_id asc";
    return query_value(conn, oss.str());
} 

uint32_t products_total(Connection& conn, uint16_t store_number, string& date, string& shelf_type) {
    ostringstream oss;
    oss << "select count(distinct(product_id)) as total_products from (select product_id, sum(assigned_shelves) "
        << "as shelves from (select p.product_id, -(p.added-p.removed) as assigned_shelves from (select product_id, "
        << "sum(case when ((facing_old>0) and (facing_new<0)) then 1 else 0 end) as removed, sum(case when ((facing_old<0) "
        << "and (facing_new>0)) then 1 else 0 end) as added, sum(case when ((facing_old)=(facing_new) and "
        << "(column_old<>column_new)) then 1 else 0 end) as rearranged from " << DATABASE << ".audit where "
        << "(updatedAt >= DATE(DATE_ADD(\'" << date << "\', interval 1 day))) and (updatedAt < "
        << "DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) and (storeNumber = \'" << store_number << "\') "
        << "and (shelf_id like \'%" <<  shelf_type << "%\') group by product_id order by product_id asc) "
        << "p union all select product_id, count(product_id) "
        << "as assigned_shelves from " << DATABASE << ".shelf_product_ref where (storeNumber = \'"
        << store_number << "\') and (shelf_id like \'%" << shelf_type << "%\') group by product_id) "
        << "u group by product_id) s where shelves > 0";
    return query_value(conn, oss.str());
}

uint32_t shelves_total(Connection& conn, uint16_t store_number, string& date, string& shelf_type) {
    ostringstream oss;
    oss << "select count(distinct(shelf_id)) as total_shelves from(select shelf_id, sum(assigned_products) "
        << "as shelves from (select p.shelf_id, -(p.added-p.removed) as assigned_products from (select shelf_id, "
        << "sum(case when ((facing_old>0) and (facing_new<0)) then 1 else 0 end) as removed, sum(case when ((facing_old<0) "
        << "and (facing_new>0)) then 1 else 0 end) as added, sum(case when ((facing_old)=(facing_new) and "
        << "(column_old<>column_new)) then 1 else 0 end) as rearranged from " << DATABASE << ".audit where "
        << "(updatedAt >= DATE(DATE_ADD(\'" << date << "\', interval 1 day))) and (updatedAt < "
        << "DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) and (storeNumber = \'" << store_number << "\') "
        << "and (shelf_id like \'%" << shelf_type << "%\') group by shelf_id order by shelf_id asc) "
        << "p union all select shelf_id, count(shelf_id) "
        << "as assigned_products from " << DATABASE << ".shelf_product_ref where (storeNumber = \'"
        << store_number << "\') and (shelf_id like \'%" << shelf_type << "%\') group by shelf_id) "
        << "u group by shelf_id) s where shelves > 0";
    return query_value(conn, oss.str());
} 

uint32_t product_lookups(Connection& conn, uint16_t store_number, string& date) {
    ostringstream oss;
    oss << "select count(id) from " << DATABASE << ".product_lookup_log where (updatedAt >= DATE(DATE_ADD(\'" << date
        << "\', interval 1 day))) and (updatedAt < DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) and (storeNumber = \'"
        << store_number << "\')";
    return query_value(conn, oss.str());
}

uint32_t shelves_modified(Connection& conn, uint16_t store_number, string& date) {
    ostringstream oss;
    oss << "select count(distinct(shelf_id)) from " << DATABASE << ".audit where (updatedAt >= "
        << "DATE(DATE_ADD(\'" << date << "\', interval 1 day))) and (updatedAt < DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) "
        << "and (storeNumber = \'" << store_number << "\')";
    return query_value(conn, oss.str());
}

uint32_t store_lookups(Connection& conn, uint16_t store_number, string& date) {
    ostringstream oss;
    oss << "select count(id) from " << DATABASE << ".store_lookup_log where (updatedAt >= DATE(DATE_ADD(\'" << date
        << "\', interval 1 day))) and (updatedAt < DATE(DATE_ADD(CURRENT_DATE(), interval 2 day))) and (storeLookedUp = \'"
        << store_number << "\')";
    return query_value(conn, oss.str());
}

vector<string> shelf_types(Connection& conn) {
    auto query = conn.query("select * from shelf_types");
    vector<string> types;
    for (auto row : query.store()) {
        types.push_back(row[0].c_str());
    }
    return types;
}

vector<int16_t> get_store_numbers(Connection& conn) {
    auto query = conn.query("select distinct(storeNumber) from audit");
    vector<int16_t> stores;
    for (auto row : query.store()) {
        stores.push_back(atoi(row[0].c_str()));
    }
    return stores;
} 

bool insert_adoption_summary(Connection& conn, daily_adoption_summary summary) {
    try {
        auto query = conn.query();
        query << "INSERT INTO store_activity_summary "
            << "(date, store_no, product_lookups, shelf_modifications, store_lookups)"
            << "VALUES (\'" << summary.date << "\', " << summary.product_lookups << ", " << summary.shelves_modified << ", "
            << summary.store_lookups << ")";
        query.execute();
        return true;
    } catch (exception e) {
        cerr << "Insertion failed. Error: " << endl;
        cerr << "Error: " << e.what() << endl;
        return false;
    }
}

bool insert_summary(Connection& conn, daily_shelf_summary summary) {
    try {
        auto query = conn.query();
        query << "INSERT INTO store_products_summary "
            << "(date, store_no, products_added, products_removed, products_modified, total_shelves, total_products, products_per_shelf, shelf_type)"
            << " VALUES (\'" << summary.date << "\', " << summary.store_number << ", " << summary.products_added << ", " << summary.products_removed
            << ", " << summary.products_rearranged << ", " << summary.total_shelves << ", " << summary.total_products << ", " << summary.products_per_shelf
            << ", \'" << summary.shelf_type << "\')";
        //cerr << query.str();
        query.execute();
        return true;
    } catch (exception e) {
        cerr << "Insertion failed" << endl;
        cerr << "Error: " << e.what() << endl;
        return false;
    }
}

int main(int argc, char *argv[])
{
    string database = DATABASE;
    string user = "root";
    string pass = "v44UFMY1zDxr@";
    ostringstream date_stream;
    auto now = time(0);
    auto *t = localtime(&now);
    date_stream << t->tm_year + 1900 << "-" << ((t->tm_mon >= 9) ? "" : "0") << 1 + t->tm_mon << "-" << t->tm_mday;
    Connection conn(false);
    if (conn.connect(database.c_str(), "localhost", user.c_str(), pass.c_str())) {
        cerr << "Connection succeeded to " << "\'localhost\'@\'" << database << "\'" << endl;
        string date = date_stream.str();
        auto shelves = shelf_types(conn);
        auto stores = get_store_numbers(conn);
        for(auto store : stores) {
            for(auto shelf : shelves) {
                daily_shelf_summary summary;
                summary.shelf_type = shelf;
                summary.store_number = store;
                summary.date = date;
                summary.products_added = products_added(conn, store, date, shelf);
                summary.products_removed = products_removed(conn, store, date, shelf);
                summary.products_rearranged = products_rearranged(conn, store, date, shelf);
                summary.total_products = products_total(conn, store, date, shelf);
                summary.total_shelves = shelves_total(conn, store, date, shelf);
                summary.products_per_shelf = (summary.total_shelves == 0) ? 0 : (summary.total_products / summary.total_shelves);
                insert_summary(conn, summary);
            }
            daily_adoption_summary adoption;
            adoption.store_lookups = store_lookups(conn, store, date);
            adoption.product_lookups = product_lookups(conn, store, date);
            adoption.shelves_modified = shelves_modified(conn, store, date);
            adoption.date = date;
            adoption.store_number = store;
            insert_adoption_summary(conn, adoption);
        }
    }
    else {
        cerr << "DB connection failed: " << conn.error() << endl;
        return 1;
    }
    mysql_library_end();
    return 0;
}
